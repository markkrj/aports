# Contributor: Vladyslav Frolov <frolvlad@gmail.com>
# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Leonardo Arena <rnalrd@alpinelinux.org>
pkgname=freeradius
pkgver=3.0.20
pkgrel=4
pkgdesc="RADIUS (Remote Authentication Dial-In User Service) server"
url="https://freeradius.org/"
arch="all"
license="GPL-2.0-or-later"
makedepends="
	autoconf
	automake
	bash
	curl-dev
	gdbm-dev
	hiredis-dev
	json-c-dev
	krb5-dev
	libpcap-dev
	libtool
	linux-headers
	linux-pam-dev
	mariadb-connector-c-dev
	net-snmp-tools
	openldap-dev
	openssl-dev
	perl-dev
	postgresql-dev
	python3-dev
	readline-dev
	sqlite-dev
	talloc-dev
	unixodbc-dev
	"
pkggroups="radius"
pkgusers="radius"
install="$pkgname.pre-install $pkgname.post-upgrade"
subpackages="
	$pkgname-dbg
	$pkgname-doc
	$pkgname-static
	$pkgname-dev
	$pkgname-eap
	$pkgname-ldap
	$pkgname-dhcp
	$pkgname-lib
	$pkgname-mssql
	$pkgname-mysql
	$pkgname-sql
	$pkgname-perl
	$pkgname-postgresql
	$pkgname-python3
	$pkgname-radclient
	$pkgname-sqlite
	$pkgname-unixodbc
	$pkgname-pam
	$pkgname-krb5
	$pkgname-rest
	$pkgname-redis
	$pkgname-checkrad
	$pkgname-utils
	"
provides="freeradius3=$pkgver-r$pkgrel"
source="ftp://ftp.freeradius.org/pub/freeradius/$pkgname-server-$pkgver.tar.gz
	$pkgname.confd
	$pkgname.initd

	musl-fix-headers.patch
	fix-scopeid.patch
	default-config.patch
	Fix-permissions-of-certs-in-bootstrap-fallback.patch
	fix-request_running-segfault.patch
	"
builddir="$srcdir/$pkgname-server-$pkgver"

# secfixes:
#   3.0.19-r3:
#     - CVE-2019-10143
#   3.0.19-r0:
#     - CVE-2019-11234
#     - CVE-2019-11235

_radconfdir="/etc/raddb"
_radmodsdir="$_radconfdir/mods-available"
_radlibdir="/usr/lib/freeradius"
_radmodsconfdir="$_radconfdir/mods-config"
ldpath="$_radlibdir"

prepare() {
	default_prepare
	update_config_sub
	# remove certs generation
	# rm -rf raddb/certs
}

build() {
	# freeradius requries json.h to be in a dir called 'json'. We fool
	# the configure script with a symlink pointing to proper location.
	ln -s /usr/include/json-c json

	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--datarootdir=/usr/share \
		--libdir="$_radlibdir" \
		--with-logdir=/var/log/radius \
		--with-radacctdir=/var/log/radius/radacct \
		--with-system-libtool \
		--with-system-libltdl \
		--with-shared-libs \
		--with-udpfromto \
		--with-rlm_sql_sqlite \
		--with-rlm_sql_postgresql \
		--with-rlm_sql_mysql \
		--with-rlm_krb5 \
		--with-rlm_rest \
		--with-rlm_redis \
		--with-rlm_rediswho \
		--with-modules="rlm_python3" \
		--without-rlm_eap_tnc \
		--without-rlm_eap_ikev2 \
		--without-rlm_sql_iodbc \
		--without-rlm_sql_oracle \
		--without-rlm_yubikey \
		--without-rlm_ykclient \
		--with-jsonc-include-dir="$PWD"

	make -j1 LDFLAGS="$LDFLAGS -lssl"
}

package() {
	install -d -m0750 -o root -g radius \
		"$pkgdir"$_radconfdir

	install -d -m0750 -o radius -g radius \
		"$pkgdir"/var/lib/radiusd \
		"$pkgdir"/var/log/radius \
		"$pkgdir"/var/log/radius/radacct

	PACKAGE=yes make -j1 R="$pkgdir" install

	chown -R root:radius "$pkgdir"/etc/raddb/*

	install -m755 -D "$srcdir"/$pkgname.initd "$pkgdir"/etc/init.d/radiusd
	install -m644 -D "$srcdir"/$pkgname.confd "$pkgdir"/etc/conf.d/radiusd

	# Install misses to create this
	mkdir -p "$pkgdir"$_radmodsconfdir/sql/ippool-dhcp/postgresql

	# Remove unneeded and unused stuff (e.g. for disabled modules).

	rm -f "$pkgdir"/usr/sbin/rc.radiusd
	rm -f "$pkgdir"$_radlibdir/rlm_test.so
	rm -f "$pkgdir"$_radconfdir/experimental.conf

	# https://github.com/FreeRADIUS/freeradius-server/issues/1734#issuecomment-247848277
	rm -f "$pkgdir"/usr/bin/dhcpclient
	rm -f "$pkgdir"/usr/share/man/man1/dhcpclient.1*

	cd "$pkgdir"$_radmodsdir
	rm -f couchbase python unbound yubikey

	cd "$pkgdir"$_radmodsconfdir
	rm -rf sql/*/mongo
	rm -rf sql/*/oracle
	rm -rf unbound

	cd "$pkgdir"$_radconfdir/sites-available
	rm -f abfab* *.orig

	cd "$pkgdir"$_radconfdir/policy.d
	rm -f abfab*
}

eap() {
	pkgdesc="EAP module for FreeRADIUS server"
	depends="freeradius=$pkgver-r$pkgrel"
	provides="freeradius3-eap=$pkgver-r$pkgrel"

	amove $_radlibdir/rlm_eap*.so $_radlibdir/libfreeradius-eap.so
	amove usr/bin/radeapclient

	amove $_radmodsdir/eap $_radmodsdir/inner-eap
	amove $_radconfdir/mods-enabled/eap
	amove $_radconfdir/sites-available/check-eap-tls
}

ldap() {
	pkgdesc="LDAP module for FreeRADIUS server"
	depends="freeradius=$pkgver-r$pkgrel"
	provides="freeradius3-ldap=$pkgver-r$pkgrel"

	amove $_radlibdir/rlm_ldap*
	amove $_radmodsdir/ldap
}

krb5() {
	pkgdesc="Kerberos module for FreeRADIUS server"
	depends="freeradius=$pkgver-r$pkgrel"
	provides="freeradius3-krb5=$pkgver-r$pkgrel"

	amove $_radlibdir/rlm_krb5*
	amove $_radmodsdir/krb5
}

dhcp() {
	pkgdesc="DHCP module for FreeRADIUS server"
	depends="freeradius=$pkgver-r$pkgrel"

	amove $_radlibdir/*_dhcp.so $_radlibdir/libfreeradius-dhcp.so
	amove $_radmodsdir/dhcp
	amove $_radconfdir/sites-available/dhcp
}

lib() {
	pkgdesc="Freeradius shared libraries"
	depends=""

	amove $_radlibdir/libfreeradius-*.so
	amove usr/share/freeradius/*
}

sql() {
	pkgdesc="SQL module for FreeRADIUS server"
	depends="freeradius=$pkgver-r$pkgrel"
	provides="freeradius3-sql=$pkgver-r$pkgrel"

	local lib; for lib in sql sqlippool sql_null sqlcounter; do
		amove $_radlibdir/rlm_$lib.so
	done

	amove $_radconfdir/sites-available/buffered-sql
	amove $_radmodsdir/*sql*
}

mysql() {
	pkgdesc="MySQL module for FreeRADIUS server"
	depends="freeradius-sql=$pkgver-r$pkgrel"
	provides="freeradius3-mysql=$pkgver-r$pkgrel"

	_mvdb mysql
}

mssql() {
	pkgdesc="MSSQL module for FreeRADIUS server"
	depends="freeradius-sql=$pkgver-r$pkgrel"
	provides="freeradius3-mssql=$pkgver-r$pkgrel"

	amove $_radmodsconfdir/sql/main/mssql
}

perl() {
	pkgdesc="Perl module for FreeRADIUS server"
	depends="freeradius=$pkgver-r$pkgrel perl"
	provides="freeradius3-perl=$pkgver-r$pkgrel"

	amove $_radlibdir/rlm_perl*
	amove $_radconfdir/mods-available/perl
	amove $_radmodsconfdir/perl
}

checkrad() {
	pkgdesc="Check if a user is (still) logged in on a certain port"
	depends="perl perl-net-telnet perl-snmp-session net-snmp-tools"

	amove usr/sbin/checkrad
}

postgresql() {
	pkgdesc="PostgreSQL module for FreeRADIUS server"
	depends="freeradius-sql=$pkgver-r$pkgrel"
	provides="freeradius3-postgresql=$pkgver-r$pkgrel"

	_mvdb postgresql
}

python3() {
	depends="freeradius=$pkgver-r$pkgrel"
	pkgdesc="Python 3 module for FreeRADIUS server"

	amove $_radlibdir/rlm_python*
	amove $_radmodsdir/python3
	amove $_radmodsconfdir/python3
}

radclient() {
	pkgdesc="Client for FreeRADIUS server"
	depends=""
	provides="freeradius3-radclient=$pkgver-r$pkgrel"

	amove usr/bin/radclient
}

sqlite() {
	pkgdesc="SQLite module for FreeRADIUS server"
	depends="freeradius-sql=$pkgver-r$pkgrel"
	provides="freeradius3-sqlite=$pkgver-r$pkgrel"

	_mvdb sqlite
}

unixodbc() {
	pkgdesc="ODBC module for FreeRADIUS server"
	depends="freeradius=$pkgver-r$pkgrel"
	provides="freeradius3-unixodbc=$pkgver-r$pkgrel"

	amove $_radlibdir/rlm_sql_unixodbc.so
}

pam() {
	pkgdesc="PAM module for FreeRADIUS server"
	depends="freeradius=$pkgver-r$pkgrel"
	provides="freeradius3-pam=$pkgver-r$pkgrel"

	amove $_radlibdir/rlm_pam*
	amove $_radmodsdir/pam
}

rest() {
	pkgdesc="REST module for FreeRADIUS server"
	depends="freeradius=$pkgver-r$pkgrel"

	amove $_radlibdir/rlm_rest*
	amove $_radmodsdir/rest
}

redis() {
	pkgdesc="Redis modules for FreeRADIUS server"
	depends="freeradius=$pkgver-r$pkgrel"

	amove $_radlibdir/rlm_redis*
	amove $_radmodsdir/redis*
}

utils() {
	pkgdesc="FreeRADIUS utilities"
	depends="freeradius=$pkgver-r$pkgrel"

	amove usr/bin/*
}

_mvdb() {
	amove $_radmodsconfdir/sql/*/$1
	amove $_radlibdir/rlm_sql_$1.so
}

sha512sums="513ed0a5d9e6b9a8d89a9b02c86ff528a9ff14d928f4c1040ca44702465abd711588fe6afa35554cb2c8e8bd7f19dd5be3dbc78445c62c7b00bf5cbc4c621312  freeradius-server-3.0.20.tar.gz
e248159c0a44f722e405c51c8015d9ad672e42ad0d38ca28f8a051ff911aa4d3e630b9bd4543e9d610940bc4ae50c022594e219ce341b36abe85c572acad418b  freeradius.confd
ba3c424d4eabb147c7aa3e31575a87ddb26b6a792d2a8714e73d8763e07854326a03a83991a7420246ca06bf0b93d0a6f23ec198f5e48647f9d25b40067e852a  freeradius.initd
c49e5eec7497fccde5fd09dba1ea9b846e57bc88015bd81640aa531fb5c9b449f37136f42c85fe1d7940c5963aed664b85da28442b388c9fb8cc27873df03b2d  musl-fix-headers.patch
41d478c0e40ff82fc36232964037c1ab8ffca9fdbb7dca02ed49319906e751c133b5d7bc7773c645cec6d9d39d1de69cba25e8d59afa8d6662563dd17f35f234  fix-scopeid.patch
14fdb7176a6e76bb078f71ae494da4f675bcdd4a90cac7ac17cc34fab5107b1faef7b37c5551917e9fb81ab26fb4e2186e23766ac2bba06db911d4b3cd7fad0b  default-config.patch
f88cb4ae335d67211c8563b6df88e20ee3729e57aa56423f99b518f83b190479b38bb189a0ab53c70ef9709a6229ccaa506ea6b79844cbfd4f2a7f0c7c292045  Fix-permissions-of-certs-in-bootstrap-fallback.patch
7ddf75901f635216b0d972c14631334a8138e0dbb021685bb6b3a996f38d232b84146c621dae541b00f6149fa401e835d1579bbacd27fad72a80bacd4391b404  fix-request_running-segfault.patch"
